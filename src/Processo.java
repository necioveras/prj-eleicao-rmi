import java.io.Serializable;
import java.lang.management.ManagementFactory;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;


public class Processo implements Runnable, Serializable, IEleicao {			
	
	private static final long serialVersionUID = 1L;
	private String pid; 
	private List<IEleicao> peers; 
	private IServidor servidor = null; 
	private boolean exported = false;
	private int pidCoordinatior; 
	private boolean leader; 
	
	public Processo(){
		pid = ManagementFactory.getRuntimeMXBean().getName().split("@")[0];
		peers = new LinkedList<>();
		pidCoordinatior = 0;
		leader = false;
	}

	@Override
	public void run() {
		
		try {
			while(true){
				if (exported == false){
					System.out.println("Oi...sou: " + pid + " e vou registrar-me no servidor. ");
					exported = export();	
				}
				
				if (leader)
					System.out.println("Sou o atual coordenador!");
				else if (pidCoordinatior != 0){
					System.out.println("O coordenador atual eh: " + pidCoordinatior);
					if (servidor.checkProcesses(pidCoordinatior) == false){ //verifica os processos
						System.out.println("Coordenador inativo ... iniciando imediatamente uma nova eleicao!");
						startElection();
					}
				}								
				
				Thread.sleep(new Random().nextInt(5000)); //tempo aleatorio de no maximo 5s
				System.out.println("Buscando os pares...");
				findMyPeers();
				if (peers.size() > 0)
					for (IEleicao p: peers)
						System.out.println("Pid:" + p.getPid());
				
				int t = new Random().nextInt(30000) + 30000;
				System.out.println("Aguardando ... " + t + "s para iniciar uma nova eleicao");
				Thread.sleep(t);
				startElection();
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException r){
			System.out.println("Algum objeto deixou de ficar disponível");
			System.out.println(r);
		}
		
	}
		
	
	public String getPid() {
		return pid;
	}



	public void setPid(String pid) {
		this.pid = pid;
	}
	
	private boolean export(){
		try {
		      // Exportar o objeto
		      UnicastRemoteObject.exportObject(this,0);		      
		    }
		    catch (RemoteException e) {
		      System.out.println("Nao consegui exportar o Processo - " + pid);
		      System.out.println(e);
		      System.exit(0);
		    }
		    try {
		      // Obter a referencia do Servidor
		      servidor = (IServidor) Naming.lookup("rmi://localhost:8099/Servidor");
		      servidor.registro(this);
		      return true;
		    }
		    catch (RemoteException e) {
		      System.out.println("Nao consegui registrar o Processo - "+ pid +" no servidor");
		      System.out.println(e.toString());
		      return false;
		    }
		    catch (MalformedURLException mal){
		    	System.out.println(mal.toString());
		    	return false;
		    }
		    catch (NotBoundException not){
		    	System.out.println(not.toString());
		    	return false;
		    }
	}	
	
	private void findMyPeers(){
		
		if (servidor != null){
			try {
				IEleicao[] peers = servidor.descobrir();
				
				this.peers.clear();
				
				for (IEleicao p: peers)
					if (!p.getPid().equals(getPid()))
						this.peers.add(p);
				
			} catch (RemoteException e) {
				System.out.println("Nao consegui encontrar meus pares.");
				System.out.println(e);
			}
		}			
		
	}



	public static void main(String[] args) {
		Processo p1 = new Processo();
		Thread t1 = new Thread(p1);		
		t1.start();		
	}

	@Override
	public void startElection() throws RemoteException {
		servidor.election(this);
	}

	@Override
	public void setLeader() throws RemoteException {
		leader = true;
	}

	@Override
	public void setCoordinator(int pid) throws RemoteException {
		pidCoordinatior = pid;		
	}

	@Override
	public boolean isAlive() throws RemoteException {
		return true;
	}

	@Override
	public boolean election(IEleicao e) throws RemoteException {
		// TODO Auto-generated method stub
		return false;
	}

}
