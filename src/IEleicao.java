import java.rmi.Remote;
import java.rmi.RemoteException;


public interface IEleicao extends Remote {
	
	public  String getPid() throws RemoteException;
	public  void   startElection() throws RemoteException;
	public  void   setLeader() throws RemoteException;
	public  void   setCoordinator(int pid) throws RemoteException;
	public  boolean isAlive() throws RemoteException;
	public  boolean election (IEleicao e) throws RemoteException;

}
