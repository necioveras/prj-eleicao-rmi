import java.rmi.Remote;
import java.rmi.RemoteException;


public interface IServidor extends Remote{
	
	public IEleicao[] descobrir() throws RemoteException;
	public void election (IEleicao processo) throws RemoteException;
	public void registro(IEleicao processo) throws RemoteException;
	public boolean checkProcesses(int pidCoordinador) throws RemoteException; 

}
