import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import java.util.List;


public class Servidor implements IServidor, Runnable{
	
	private List<IEleicao> processos; // lista de todos os processos registrados

	public Servidor(){
		processos = new LinkedList<>();
	}
	
	public void run(){
		/*try {
			Thread.currentThread();
			
			
			
			Thread.sleep(1000); // sleep for a second
		} 
		catch (InterruptedException e) {
			System.out.println(e);
		}*/
	}
	
	@Override
	public IEleicao[] descobrir() throws RemoteException {
		IEleicao[] ps = null;
		if (processos.size() > 0){
			ps = new IEleicao[processos.size()];
			for (int i = 0; i < ps.length; i++)
				ps[i] = processos.get(i);				
		}		
		return ps;
	}

	@Override
	public void registro(IEleicao processo) throws RemoteException {
		processos.add(processo);		
	}
	
	public static void main(String[] args) {
		try {
			System.out.println("Criando o servidor de registry...");
			Registry reg = null;
			try {
				reg = LocateRegistry.createRegistry(8099);
			} catch (Exception ex) {
				try {
					reg = LocateRegistry.getRegistry(8099);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			System.out.println("Criando servidor...");
			Servidor serv = new Servidor();
			IServidor stub_servidor = (IServidor) UnicastRemoteObject.exportObject(serv,0);
			System.out.println("Ligando servidor ao Registry...");
			// Naming.rebind("rmi://localhost:8099/Servidor", servidor);
			reg.rebind("Servidor", stub_servidor);
			System.out.println("Servidor ligado ao Registry");
		} catch (Exception e) {
			System.out.println("Problema ao conectar servidor no Regitry (binding)");
			System.out.println(e.toString());
		}
	}

	@Override
	public void election(IEleicao processo) throws RemoteException {
		try{
			int pid = Integer.parseInt(processo.getPid());
			for (IEleicao e: processos){
				int i = Integer.parseInt(e.getPid());
				if (i > pid)
					pid = i;
			}		
			for (IEleicao e: processos)
				if (Integer.parseInt(e.getPid()) == pid)
					e.setLeader();
				else
					e.setCoordinator(pid);
		} catch (RemoteException r){
			System.out.println("Alguem nao responde...");
			System.out.println(r);
		}
	}

	@Override
	public boolean checkProcesses(int pidCoordinador) throws RemoteException {
		boolean ok = false;
		for (IEleicao e: processos){
			try{
				e.isAlive();
				if (Integer.parseInt(e.getPid()) == pidCoordinador)
					ok = true;
			} catch(RemoteException r){
				processos.remove(e);
			}
		}		
		return ok;
	}

}