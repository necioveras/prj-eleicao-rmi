import java.io.Serializable;
import java.lang.management.ManagementFactory;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class SingleProcess implements Runnable, Serializable, IEleicao {
	
	private static final long serialVersionUID = 1L;
	private String pid; 
	private List<IEleicao> peers;  
	private boolean exported = false;
	private int pidCoordinatior; 
	private boolean leader; 
	private Registry reg = null;
	
	public SingleProcess() {
		pid = ManagementFactory.getRuntimeMXBean().getName().split("@")[0];
		peers = new LinkedList<>();		
		pidCoordinatior = 0;
		leader = false;
		peers.add(this);
	}

	@Override
	public String getPid() throws RemoteException {
		return pid;
	}

	@Override
	public void startElection() throws RemoteException {		
		try{
			for (IEleicao e: peers)
				if (Integer.parseInt(e.getPid()) > Integer.parseInt(getPid()))
					if (e.election(this) == true){
						leader = false;
						return;
					}
			
			setLeader(); //ninguem respondeu
			
		} catch (RemoteException r){
			System.out.println("Alguem nao responde...");
			System.out.println(r);
		}		
	}
	
	@Override
	public  boolean election (IEleicao e) throws RemoteException{
		if (Integer.parseInt(e.getPid()) > Integer.parseInt(getPid()))
			return false;
		else{
			setLeader();
			return true;
		}
	}

	@Override
	public void setLeader() throws RemoteException {
		leader = true;		
		pidCoordinatior = Integer.parseInt(getPid());
		comunicateCoordinator(pidCoordinatior);
	}

	@Override
	public void setCoordinator(int pid) throws RemoteException {
		pidCoordinatior = pid;	
		if (pid != Integer.parseInt(getPid()))
			leader = false;
	}

	@Override
	public boolean isAlive() throws RemoteException {
		return true;
	}

	@Override
	public void run() {		
		try {
			while(true){
				if (exported == false){
					System.out.println("Oi...sou: " + pid + " e vou registrar-me no servidor. ");
					registry();	
				}
				
				if (leader)
					System.out.println("Sou o atual coordenador!");
				else if (pidCoordinatior != 0){
					System.out.println("O coordenador atual eh: " + pidCoordinatior);
					if (checkProcesses(pidCoordinatior) == false){ //verifica os processos
						System.out.println("Coordenador inativo ... iniciando imediatamente uma nova eleicao!");
						updateAndViewMyPeers();
						startElection();
					}
				}								
				
				Thread.sleep(new Random().nextInt(5000)); //tempo aleatorio de no maximo 5s
				//updateAndViewMyPeers();
				
				int t = new Random().nextInt(30000) + 30000;
				System.out.println("Aguardando ... " + t + "s para decidir sobre o inicio de uma nova eleicao");
				Thread.sleep(t);
				updateAndViewMyPeers();
				if (pidCoordinatior == 0){
					System.out.println("Iniciando uma nova eleicao...");
					startElection();
				}
				else{
					System.out.println("Sem eleicao. Ja temos um coordenador: " + pidCoordinatior);
				}
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException r){
			System.out.println("Algum objeto deixou de ficar disponível");
			System.out.println(r);
		}
	}
	
	private void updateAndViewMyPeers(){
		System.out.println("Buscando os pares...");
		findMyPeers();
		try{
			if (peers.size() > 0)
				for (IEleicao p: peers)
					System.out.println("Pid:" + p.getPid());
		} catch (RemoteException r){
			System.out.println("Nao consegui atualizar meus pares. Alguem esta off");
			System.out.println(r);
		}
	}
	
	private void registry(){
		try {
			System.out.println("Criando/registrando um registry...");			
			try {
				reg = LocateRegistry.createRegistry(8099);
			} catch (Exception ex) {
				try {
					reg = LocateRegistry.getRegistry(8099);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}									
			IEleicao stub_servidor = (IEleicao) UnicastRemoteObject.exportObject(this,0);
			System.out.println("Ligando-se ao Registry...");
			reg.rebind("P" + getPid(), stub_servidor);
			System.out.println("Ligado ao Registry");
			exported = true;
		} catch (Exception e) {
			System.out.println("Problema ao conectar servidor no Regitry (binding)");
			System.out.println(e.toString());
		}
	}	
	
	public boolean checkProcesses(int pidCoordinador) throws RemoteException {
		boolean ok = false;
		synchronized (peers) {
			for (IEleicao e: peers){
				try{
					e.isAlive();
					if (Integer.parseInt(e.getPid()) == pidCoordinador)
						ok = true;
				} catch(RemoteException r){
					//peers.remove(e);
					System.out.println("Algum processo nao responde!");
				}
			}	
		}			
		return ok;
	}
	
	private void findMyPeers(){
		
		String name = null;
		
		try {
			peers.clear();
			for(String s: reg.list()){
				name = s;
				IEleicao e = (IEleicao) reg.lookup(s);
				this.peers.add(e);
			}						
			
		} catch (RemoteException e) {
			System.out.println("Nao consegui encontrar meus pares.");
			System.out.println(e);		
			try {
				reg.unbind(name);
			} catch (RemoteException | NotBoundException e1) {
				System.out.println("Nao consegui exluir o processo " + name);
			}
		} catch (NotBoundException b){
			System.out.println("Conversao invalida");
			System.out.println(b);
		}
		
	}
	
	private void comunicateCoordinator(int pid){
		try{
			for (IEleicao e : peers)
				e.setCoordinator(pid);
		} catch (RemoteException r){
			System.out.println("Nao conseguir divulgar o coordenador. Alguem esta off");
			System.out.println(r);
		}
			
	}
	
	public static void main(String[] args) {
		SingleProcess p1 = new SingleProcess();
		Thread t1 = new Thread(p1);		
		t1.start();		
	}

}
